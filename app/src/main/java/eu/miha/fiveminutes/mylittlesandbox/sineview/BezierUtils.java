package eu.miha.fiveminutes.mylittlesandbox.sineview;

import android.graphics.Path;
import android.graphics.PointF;
import android.support.annotation.Nullable;

public final class BezierUtils {

    public static Path calculateBezier(PointF[] points) {
        return calculateBezier(points, false);
    }

    public static Path calculateBezier(PointF[] points, boolean closed) {
        return calculateBezier(points, closed, null);
    }

    public static Path calculateBezier(final PointF[] points, final boolean closed, @Nullable Path path) {
        if (path == null) {
            path = new Path();
        } else {
            path.reset();
        }
        PointF[] divided = CatmullRomSplineUtils.subdividePoints(points, 10, closed);
        path.moveTo((float) divided[0].x, (float) divided[0].y);
        for (int i = 0; i < divided.length - 2; i++) {
            path.quadTo((float) divided[i + 1].x, (float) divided[i + 1].y, (float) divided[i + 2].x, (float) divided[i + 2].y);
        }
        return path;
    }
}
