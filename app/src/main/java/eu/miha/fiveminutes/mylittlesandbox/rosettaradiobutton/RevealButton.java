package eu.miha.fiveminutes.mylittlesandbox.rosettaradiobutton;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import butterknife.Bind;
import butterknife.ButterKnife;
import eu.miha.fiveminutes.mylittlesandbox.R;

public final class RevealButton extends FrameLayout {

    @Bind(R.id.reveal_fill_view)
    RevealFillView revealFillView;

    @Bind(R.id.check_mark_image_view)
    ImageView checkMarkImageView;

    @Bind(R.id.gender_text)
    ColorChangingTextView genderText;

    public RevealButton(final Context context) {
        this(context, null);
    }

    public RevealButton(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RevealButton(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        final View view = inflate(context, R.layout.reveal_button, this);
        ButterKnife.bind(view);
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            handleClick(event.getX(), event.getY());
        }
        return true;
    }

    public void handleClick(final float x, final float y) {
        final boolean isChecked = checkMarkImageView.getVisibility() == VISIBLE;
        if (isChecked) {
            deactivate();
        } else {
            activate(x, y);
        }
    }

    private void activate(final float x, final float y) {
        checkMarkImageView.setVisibility(VISIBLE);
        revealFillView.startFillAnimation(x, y);
        genderText.activate();
    }

    private void deactivate() {
        checkMarkImageView.setVisibility(GONE);
        genderText.deactivate();
        revealFillView.startHideAnimation();
    }

    @Override
    public boolean isSelected() {
        return (checkMarkImageView.getVisibility() == VISIBLE);
    }
}
