package eu.miha.fiveminutes.mylittlesandbox.sineview;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

import eu.miha.fiveminutes.mylittlesandbox.R;

public final class SineView extends View {

    private static final int MESSAGE_DRAW_FRAME = 1423;
    private static final int REFRESH_RATE_MS = 16;
    private static final int DEFAULT_SINE_POINTS_COUNT = 12;
    private static final int DEFAULT_SINE_STROKE_WIDTH = 2;
    private static final int DEFAULT_SINE_BLUR_STROKE_WIDTH = 6;
    private static final int BLUR_STROKE_RADIUS = 4;
    private static final int OPAQUE = 255;
    private static final int DEFAULT_ANIMATION_DURATION_MS = 300;

    private final Handler drawHandler;

    private final Paint sinePaint;
    private final Paint blurPaint;

    private SinePoint[] sinePoints;
    private PointF[] drawPoints;

    private int sinePointsCount = DEFAULT_SINE_POINTS_COUNT;
    private int sineAnimationDuration = DEFAULT_ANIMATION_DURATION_MS;
    private float strokeWidth = DEFAULT_SINE_STROKE_WIDTH;
    private float blurStrokeWidth = DEFAULT_SINE_STROKE_WIDTH;
    private boolean autoStart;

    public SineView(final Context context) {
        this(context, null);
    }

    public SineView(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SineView(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        sinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        sinePaint.setStrokeWidth(DEFAULT_SINE_STROKE_WIDTH);
        sinePaint.setStyle(Paint.Style.STROKE);
        sinePaint.setStrokeJoin(Paint.Join.ROUND);
        sinePaint.setStrokeCap(Paint.Cap.ROUND);

        blurPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        blurPaint.set(sinePaint);
        blurPaint.setStrokeWidth(DEFAULT_SINE_BLUR_STROKE_WIDTH);
        blurPaint.setMaskFilter(new BlurMaskFilter(BLUR_STROKE_RADIUS, BlurMaskFilter.Blur.NORMAL));

        if (attrs != null) {
            parseAttributes(attrs);
        }

        this.drawHandler = new Handler(msg -> {
            drawSineWave();
            return false;
        });

        if (autoStart) {
            drawHandler.sendEmptyMessageDelayed(MESSAGE_DRAW_FRAME, REFRESH_RATE_MS);
        }
    }

    private void parseAttributes(final AttributeSet attrs) {
        final TypedArray typedArray = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.SineView, 0, 0);
        try {
            sinePointsCount = typedArray.getInteger(R.styleable.SineView_sinePointsCount, DEFAULT_SINE_POINTS_COUNT);
            final int sineColor = typedArray.getColor(R.styleable.SineView_sineColor, Color.BLACK);
            sinePaint.setColor(sineColor);
            blurPaint.setColor(sineColor);
            final int sineAlpha = typedArray.getInteger(R.styleable.SineView_sineAlpha, OPAQUE);
            sinePaint.setAlpha(sineAlpha);
            blurPaint.setAlpha(sineAlpha);
            strokeWidth = typedArray.getFloat(R.styleable.SineView_sineStrokeWidth, DEFAULT_SINE_STROKE_WIDTH);
            sinePaint.setStrokeWidth(strokeWidth);
            blurStrokeWidth = typedArray.getFloat(R.styleable.SineView_sineBlurStrokeWidth, DEFAULT_SINE_BLUR_STROKE_WIDTH);
            blurPaint.setStrokeWidth(blurStrokeWidth);
            sineAnimationDuration = typedArray.getInteger(R.styleable.SineView_sineAnimationDurationMs, DEFAULT_ANIMATION_DURATION_MS);
            autoStart = typedArray.getBoolean(R.styleable.SineView_sineAutoStart, false);
        } finally {
            typedArray.recycle();
        }
    }

    private void drawSineWave() {
        invalidate();
        drawHandler.sendEmptyMessageDelayed(MESSAGE_DRAW_FRAME, REFRESH_RATE_MS);
    }

    public void startAnimating() {
        autoStart = true;
        if (sinePoints == null) {
            return;
        }
        for (final SinePoint sinePoint : sinePoints) {
            sinePoint.startAnimation();
        }
        drawSineWave();
    }

    public void stopAnimating() {
        drawHandler.removeMessages(MESSAGE_DRAW_FRAME);
        for (final SinePoint sinePoint : sinePoints) {
            sinePoint.stopAnimating();
        }
    }

    @Override
    protected void onSizeChanged(final int newWidth, final int newHeight, final int oldw, final int oldh) {
        super.onSizeChanged(newWidth, newHeight, oldw, oldh);

        final int heightPadding = (int) ((strokeWidth / 2) + (blurStrokeWidth / 2));

        generateSinePoints(sinePointsCount, newWidth, newHeight, heightPadding);
    }

    private void generateSinePoints(final int count, final int width, final int height, final float heightPadding) {
        final int half = height / 2;
        final int xIncrement = (width / (count - 1));
        sinePoints = new SinePoint[sinePointsCount];
        drawPoints = new PointF[sinePointsCount];
        for (int i = 0; i < count; i++) {
            final SinePoint sinePoint = new SinePoint(xIncrement * i, half, heightPadding, height - heightPadding, i, sineAnimationDuration);
            sinePoints[i] = sinePoint;
            drawPoints[i] = sinePoint.getPointF();
            if (autoStart) {
                sinePoint.startAnimation();
            }
        }
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        drawContour(canvas, drawPoints);
    }

    private void drawContour(final Canvas canvas, final PointF[] countourPoints) {
        final Path sinePath = BezierUtils.calculateBezier(countourPoints, false);
        canvas.drawPath(sinePath, blurPaint);
        canvas.drawPath(sinePath, sinePaint);
    }

    private static final class SinePoint {

        private static final int ANIMATION_OFFSET_MS = 100;

        private final PointF pointF;
        private final ValueAnimator yAnimator;

        public SinePoint(final float x, final float y, final float minY, final float maxY, final int index, final int animationDurationMs) {
            pointF = new PointF(x, y);
            yAnimator = ValueAnimator.ofFloat(minY, maxY);
            yAnimator.setDuration(animationDurationMs);
            yAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
            yAnimator.setRepeatMode(ValueAnimator.REVERSE);
            yAnimator.setRepeatCount(ValueAnimator.INFINITE);
            yAnimator.setCurrentPlayTime(animationDurationMs / 2 + (ANIMATION_OFFSET_MS * index));
            yAnimator.addUpdateListener(animation -> updatePoint((float) animation.getAnimatedValue()));
        }

        public void startAnimation() {
            yAnimator.start();
        }

        public void stopAnimating() {
            yAnimator.cancel();
        }

        private void updatePoint(final float newY) {
            pointF.set(pointF.x, newY);
        }

        public PointF getPointF() {
            return pointF;
        }
    }
}
