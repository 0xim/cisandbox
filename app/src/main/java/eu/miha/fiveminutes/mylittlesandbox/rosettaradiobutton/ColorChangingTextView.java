package eu.miha.fiveminutes.mylittlesandbox.rosettaradiobutton;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.animation.AccelerateInterpolator;
import android.widget.TextView;

import eu.miha.fiveminutes.mylittlesandbox.R;

public class ColorChangingTextView extends TextView {

    private static final int DEFAULT_ANIMATION_DURATION = 300;

    private int activatedColor = Color.BLACK;
    private int deactivatedColor = Color.BLACK;
    private int animationDuration = DEFAULT_ANIMATION_DURATION;

    private final ValueAnimator toActivatedColorAnimator;

    public ColorChangingTextView(final Context context) {
        this(context, null);
    }

    public ColorChangingTextView(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ColorChangingTextView(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        if (attrs != null) {
            parseAttributes(attrs);
        }

        toActivatedColorAnimator = ValueAnimator.ofObject(new ArgbEvaluator(), deactivatedColor, activatedColor);
        toActivatedColorAnimator.setDuration(animationDuration);
        toActivatedColorAnimator.setInterpolator(new AccelerateInterpolator());

        toActivatedColorAnimator.addUpdateListener(animation -> setTextColor((Integer) animation.getAnimatedValue()));

        setTextColor(deactivatedColor);
    }

    private void parseAttributes(final AttributeSet attrs) {
        final TypedArray typedArray = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.ColorChangingTextView, 0, 0);
        try {
            activatedColor = typedArray.getColor(R.styleable.ColorChangingTextView_activatedColor, Color.WHITE);
            deactivatedColor = typedArray.getColor(R.styleable.ColorChangingTextView_deactivatedColor, Color.BLACK);
            animationDuration = typedArray.getInteger(R.styleable.ColorChangingTextView_changeDuration, DEFAULT_ANIMATION_DURATION);
        } finally {
            typedArray.recycle();
        }
    }

    public void activate() {
        toActivatedColorAnimator.start();
    }

    public void deactivate() {
        toActivatedColorAnimator.reverse();
    }
}
