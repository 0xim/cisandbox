package eu.miha.fiveminutes.mylittlesandbox.rosettaradiobutton;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;

import eu.miha.fiveminutes.mylittlesandbox.R;

public final class RevealFillView extends View {

    private static final int CORNER_SWEEP_ANGLE = 90;
    private static final float STROKE_WIDTH = 3.0f;

    private static final int DEFAULT_ANIMATION_DURATION = 300;

    private final Region viewRegion = new Region();
    private final Path outlinePath = new Path();
    private final Region outlineRegion = new Region();
    private final Path circlePath = new Path();
    private final Region circleRegion = new Region();
    private final RectF circleRect = new RectF();

    private final ValueAnimator opacityAnimator;

    private final Paint fillPaint;

    private int width;
    private int height;
    private int cornerRadius;
    private int cornerDiameter;

    private int circleRadius;

    private float x;
    private float y;

    private int animationDuration = DEFAULT_ANIMATION_DURATION;

    private boolean handleClick;

    public RevealFillView(final Context context) {
        this(context, null);
    }

    public RevealFillView(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RevealFillView(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        fillPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        fillPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        fillPaint.setStrokeWidth(STROKE_WIDTH);
        fillPaint.setColor(Color.BLACK);

        if (attrs != null) {
            parseAttributes(attrs);
        }

        opacityAnimator = ValueAnimator.ofFloat(1.0f, 0.0f);
        opacityAnimator.setDuration(animationDuration);
        opacityAnimator.addUpdateListener(animation -> setAlpha((float) animation.getAnimatedValue()));
    }

    private void parseAttributes(final AttributeSet attrs) {
        final TypedArray typedArray = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.RevealFillView, 0, 0);
        try {
            fillPaint.setColor(typedArray.getColor(R.styleable.RevealFillView_fillColor, Color.BLACK));
            cornerRadius = (int) typedArray.getDimension(R.styleable.RevealFillView_radius, 0.0f);
            cornerDiameter = cornerRadius * 2;
            animationDuration = typedArray.getInteger(R.styleable.RevealFillView_animationDuration, DEFAULT_ANIMATION_DURATION);
            handleClick = typedArray.getBoolean(R.styleable.RevealFillView_handleClick, false);
        } finally {
            typedArray.recycle();
        }
    }

    @Override
    protected void onSizeChanged(final int newWidth, final int newHeight, final int oldw, final int oldh) {
        super.onSizeChanged(newWidth, newHeight, oldw, oldh);
        width = newWidth;
        height = newHeight;

        final Rect viewRect = new Rect(0, 0, width, height);
        viewRegion.set(viewRect);

        createOutlinePath();
        createCirclePath();
    }

    private void createOutlinePath() {
        final RectF cornerRect = new RectF();
        outlinePath.reset();
        outlinePath.moveTo(0, height - cornerRadius);
        outlinePath.lineTo(0, cornerRadius);
        cornerRect.set(0, 0, cornerDiameter, cornerDiameter);
        outlinePath.arcTo(cornerRect, 180, CORNER_SWEEP_ANGLE);             // top-left corner
        outlinePath.lineTo(width - cornerRadius, 0);
        cornerRect.set(width - cornerDiameter, 0, width, cornerDiameter);
        outlinePath.arcTo(cornerRect, 270, CORNER_SWEEP_ANGLE);             // top-right corner
        outlinePath.lineTo(width, height - cornerRadius);
        cornerRect.set(width - cornerDiameter, height - cornerDiameter, width, height);
        outlinePath.arcTo(cornerRect, 360, CORNER_SWEEP_ANGLE);             // bottom-right corner
        outlinePath.lineTo(cornerRadius, height);
        cornerRect.set(0, height - cornerDiameter, cornerDiameter, height); // bottom-left corner
        outlinePath.arcTo(cornerRect, 90, CORNER_SWEEP_ANGLE);
        outlinePath.close();
    }

    private void createCirclePath() {
        circleRect.set(x - circleRadius, y - circleRadius, x + circleRadius, y + circleRadius);
        circlePath.reset();
        circlePath.addOval(circleRect, Path.Direction.CW);
        circlePath.close();
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        if (shouldClipCircle()) {
            drawClippedCircle(canvas);
        } else {
            drawCircle(canvas);
        }
    }

    private void drawCircle(final Canvas canvas) {
        canvas.drawOval(circleRect, fillPaint);
    }

    private void drawClippedCircle(final Canvas canvas) {
        outlineRegion.setPath(outlinePath, viewRegion);
        circleRegion.setPath(circlePath, viewRegion);
        circleRegion.op(outlineRegion, Region.Op.INTERSECT);

        final Path outline = circleRegion.getBoundaryPath();
        outline.close();
        canvas.drawPath(outline, fillPaint);
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        if (!handleClick) {
            return false;
        }

        if (event.getAction() == MotionEvent.ACTION_UP) {
            startFillAnimation(event.getX(), event.getY());
        }
        return true;
    }

    public void startFillAnimation(final float x, final float y) {
        if (opacityAnimator.isRunning()) {
            opacityAnimator.cancel();
        }
        setAlpha(1.0f);
        this.x = x;
        this.y = y;
        final int maxDrawRadius = (int) Math.sqrt(Math.pow(width, 2) + Math.pow(height, 2)) + 10;
        final ValueAnimator radiusAnimator = ValueAnimator.ofInt(0, maxDrawRadius);
        radiusAnimator.setDuration(animationDuration);
        radiusAnimator.setInterpolator(new AccelerateInterpolator());
        radiusAnimator.addUpdateListener(animation -> {
            circleRadius = (int) animation.getAnimatedValue();
            createCirclePath();
            invalidate();
        });
        radiusAnimator.start();
    }

    public void startHideAnimation() {
        opacityAnimator.start();
    }

    private boolean shouldClipCircle() {
        return ((x + circleRadius > width) || (y + circleRadius > height) || (x - circleRadius < 0) || (y - circleRadius < 0));
    }
}
