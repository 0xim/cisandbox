package eu.miha.fiveminutes.mylittlesandbox;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

public final class MainActivity extends Activity {

    public static final String TEST_TEXT = "I AM VISIBLE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView cardText = (TextView) findViewById(R.id.textView);
        final CardView cardView = (CardView) findViewById(R.id.pink_card_view);
        cardView.setOnClickListener(v -> {
            cardText.setVisibility(View.VISIBLE);
            cardText.setText(TEST_TEXT);
        });
    }
}
