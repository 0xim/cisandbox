package eu.miha.fiveminutes.mylittlesandbox;

import android.test.suitebuilder.annotation.SmallTest;

import org.junit.Test;
import static org.junit.Assert.*;

@SmallTest
public class ExampleUnitTest {

    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void dummyTest() {
        assertNotNull(new Object());
    }

    @Test
    public void anotherDummyTest() {
        assertTrue(!false);
    }
}