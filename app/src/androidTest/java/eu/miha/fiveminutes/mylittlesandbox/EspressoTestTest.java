package eu.miha.fiveminutes.mylittlesandbox;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@LargeTest
public final class EspressoTestTest {

    @Rule
    public ActivityTestRule<MainActivity> mainActivityActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testCardView() {
        onView(withId(R.id.pink_card_view)).check(matches(isDisplayed()));
    }

    @Test
    public void cardClickShowTextTest() {
        onView(withId(R.id.pink_card_view)).perform(click());
        onView(withId(R.id.textView)).check(matches(isDisplayed()));
    }


    @Test
    public void cardClickValidTextTest() {
        onView(withId(R.id.pink_card_view)).perform(click());
        onView(withId(R.id.textView)).check(matches(isDisplayed()));
        onView(withId(R.id.textView)).check(matches(withText(MainActivity.TEST_TEXT)));
    }
}
